import time
import random
import threading

import requests

portfolio_endpoints = ('api/v1/pokemon', 'api/v1/pokemon/idnotexists', 'notfound')

def run():
    while True:
        try:
            target = random.choice(portfolio_endpoints)
            requests.get("http://java-app:8080/%s" % target, timeout=1)
            time.sleep(range(1, 5))
        except:
            pass


if __name__ == '__main__':
    for _ in range(4):
        thread = threading.Thread(target=run)
        thread.setDaemon(True)
        thread.start()

    while True:
        time.sleep(1)
