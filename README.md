# Dashboard Sandbox
This project is designed to be a sandbox environment in which you can experiment with Prometheus and Grafana.

The included docker-compose.yml file can be used to bring up all of the components as docker containers. This requires docker and docker-compose to be installed.

```shell
$ docker-compose up
```

The main components of the demo application are:
1. A mongodb database for storing data.
2. A "backend" REST API which stores it's data in mongodb. This will be pulled from dockerhub. You can change the image in the docker-compose.yml file

The other components included for monitoring are:
1. The mongodb prometheus exporter. This allows prometheus to read metrics from mongodb.
2. The prometheus node exporter. This allows prometheus to read metrics from the host machine e.g. CPU, memory.
3. A prometheus server configured to read metrics from
    - mongodb
    - the frontend app
    - the backend app
    - performance metrics from the host machine.
3. A grafana server configured to read metrics from the prometheus server. This includes two demo dashboards:
    - A mongodb dashboard
    - A "HTTP Apps" dashboard - designed for the frontend and backend flask apps.

## Network access
The following components will be externally accessible if their ports are open (e.g. open these ports in a firewall / Security Group)

| Component | Port |
|-----------| -----|
| prometheus| 9000 |
| grafana   | 8081 |
| java-app  | 8080 |

## Starting/Stopping components with docker-compose
You can start/stop some or all of the components with docker-compose, some example useful docker-compose commands are:

Bring up everything:
```shell
$ docker-compose up
```

Bring up a subset of what's listed in docker-compose.yml:
```shell
$ docker-compose up mongodb java-app prometheus grafana
```

Stop an individual component (e.g. mongodb):
```shell
$ docker-compose stop mongodb
```

Start an individual component (e.g. mongodb):
```shell
$ docker-compose start mongodb
```

Check what's running:
```shell
$ docker-compose ps
```

