## Part 1- Setup

NOTE: all of the commands in this step are in a single script that you can run on your linux machine with:
    

    curl -L https://bit.ly/sre-lab-setup | sh


The commands that the above script runs are:

1. Shutdown Jenkins on this machine. We are not using it on this machine, and it is taking up port 8080:
```
    sudo systemctl stop jenkins
```
2. Install docker-compose:
```
    sudo curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
    sudo chmod a+x /usr/local/bin/docker-compose
```
3. Clone the monitoring bitbucket repo
```
    git clone https://bitbucket.org/fcallaly/monitoring-java-mongodb.git  
```

## Part 2 - Running the app

1. Take a look at the docker-compose.yml file. This is starting 5 docker containers that interact with each other.
```
    cd monitoring-java-mongodb
    cat docker-compose.yml
```
2. To start the apps:
```
    docker-compose up
```
3. You should now be able to open the web pages for 3 of the containers started by docker-compose in a browser:
    
    | Application | Web Browser URL |
    |-------------|------------------|
    |Spring Boot REST API|   http://rbcgraddockerX.machinefor.me:8080|
    |Prometheus Server|      http://rbcgraddockerX.machinefor.me:9000|
    |Grafana Server|         http://rbcgraddockerX.machinefor.me:8081|